## Overview

Demo application showing how to selectively determine the language of an input text by first checking the HTTP header, then falling back on a language detection library. On receipt of a POST request containing a message, the app will first check for the existence of a `Content-Language` header in the HTTP request. If this header is found, then it is assumed to contain the correct language code for the text and will return this value. If the `Content-Language` header is not present, one of a number of langauge detection libraries will be used instead as determined by the endpoint to which the request was sent.

Links are provided for each of the libraries used. The following StackOverflow post also contains a helpful summary for each: [Read StackOverflow](https://stackoverflow.com/a/47106810/4261231)

Relevant code is in `app/rest/routes.py`.

The language model for fasttext is stored in `app/static/models/fasttext`. The location of this model is configured in `config.py`. You will need to download this model yourself from the following location: [Model](https://dl.fbaipublicfiles.com/fasttext/supervised-models/lid.176.ftz)

All other code is boilerplate Flask.

## Setup

Create and activate new virtual environment

```bash
python3 -m venv venv
source venv/bin/activate
```

Install requirements
```bash
pip install -r requirements.txt
```

## Launch Application

```python
flask run
```

## Available Language Detection Endpoints

+ `/textblob` [Project Webpage](https://textblob.readthedocs.io/en/dev/)
+ `/polyglot` [Project Webpage](https://polyglot.readthedocs.io/en/latest/)
+ `/guess-language` [Project Webpage](https://pypi.org/project/guess_language-spirit/)
+ `/langid` [Project Webpage](https://pypi.org/project/langid/)
+ `/fasttext` [Project Webpage](https://fasttext.cc/docs/en/python-module.html) [Model Used](https://dl.fbaipublicfiles.com/fasttext/supervised-models/lid.176.ftz)
+ `/cld3` [Project Webpage](https://pypi.org/project/pycld3/)

## Sample Requests

Post a document and specify the language in the request

```bash
curl -X POST -d '{"message": "Esto es en Español"}' -H 'Content-Type: application/json' -H 'Content-Language: en' http://localhost:5000/polyglot
```

Automatically detect language using Polyglot

```bash
curl -X POST -d '{"message": "Esto es en Español"}' -H 'Content-Type: application/json' http://localhost:5000/polyglot
```

Automatically detect language using fasttext

```bash
curl -X POST -d '{"message": "Esto es en Español"}' -H 'Content-Type: application/json' http://localhost:5000/fasttext
```