import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))

class Config(object):
    FASTTEXT_MODEL = os.path.join(basedir, "app", "static", "models", "fasttext", "lid.176.ftz")
