from app.rest import bp
from flask import request, jsonify, current_app as app
from guess_language import guess_language
from polyglot.detect import Detector    
from textblob import TextBlob

import cld3
import fasttext
import json
import langid
import re

# dirty hack to make fasttext model available during requests without having to
# reload the model each time
fasttext_model = None

def textblob_detect(message):    
	return TextBlob(message).detect_language()

def polyglot_detect(message):
	languages = sorted(Detector(message).languages, key=lambda x: -x.confidence)
	return languages[0].code

def guess_language_detect(message):
	return guess_language(message)

def langid_detect(message):
	return langid.classify(message)[0]

def fasttext_detect(message):
	# dirty hack to load fasttext model on first request to server
	global fasttext_model

	if fasttext_model is None:
		fasttext_model = fasttext.load_model(app.config["FASTTEXT_MODEL"])

	return re.sub("__label__", "", fasttext_model.predict(message, k=1)[0][0])

def cld3_detect(message):
	return cld3.get_language(message).language

def detect_language(detector):
	# Check if we've been told the language in the http header
	language = request.headers.get('Content-Language')
	
	if language is not None:
		return language

	# if language is not specified in the header, fall back on
	# a language detection library
	payload = json.loads(request.data.decode('utf-8'))
	message = payload['message']
	return detector(message)

@bp.route('/textblob', methods = ['POST'])
def demo_textblob():
	return jsonify({ "language": detect_language(textblob_detect) })

@bp.route('/polyglot', methods = ['POST'])
def demo_polyglot():
	return jsonify({ "language": detect_language(polyglot_detect) })

@bp.route('/guess-language', methods = ['POST'])
def demo_guess_language():
	return jsonify({ "language": detect_language(guess_language_detect) })

@bp.route('/langid', methods = ['POST'])
def demo_langid():
	return jsonify({ "language": detect_language(langid_detect) })

@bp.route('/fasttext', methods = ['POST'])
def demo_fasttext():
	return jsonify({ "language": detect_language(fasttext_detect) })

@bp.route('/cld3', methods = ['POST'])
def demo_cld3():
	return jsonify({ "language": detect_language(cld3_detect) })