from flask import Flask
from config import Config

def create_app(config_class=Config):
    app = Flask(__name__)
    
    app.config.from_object(config_class)

    from app.rest import bp as rest_bp
    app.register_blueprint(rest_bp)

    return app

